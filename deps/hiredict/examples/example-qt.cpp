// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <iostream>
using namespace std;

#include <QCoreApplication>
#include <QTimer>

#include "example-qt.h"

void getCallback(redictAsyncContext *, void * r, void * privdata) {

    redictReply * reply = static_cast<redictReply *>(r);
    ExampleQt * ex = static_cast<ExampleQt *>(privdata);
    if (reply == nullptr || ex == nullptr) return;

    cout << "key: " << reply->str << endl;

    ex->finish();
}

void ExampleQt::run() {

    m_ctx = redictAsyncConnect("localhost", 6379);

    if (m_ctx->err) {
        cerr << "Error: " << m_ctx->errstr << endl;
        redictAsyncFree(m_ctx);
        emit finished();
    }

    m_adapter.setContext(m_ctx);

    redictAsyncCommand(m_ctx, NULL, NULL, "SET key %s", m_value);
    redictAsyncCommand(m_ctx, getCallback, this, "GET key");
}

int main (int argc, char **argv) {

    QCoreApplication app(argc, argv);

    ExampleQt example(argv[argc-1]);

    QObject::connect(&example, SIGNAL(finished()), &app, SLOT(quit()));
    QTimer::singleShot(0, &example, SLOT(run()));

    return app.exec();
}
