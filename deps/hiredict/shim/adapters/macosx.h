/*
 * Copyright (c) 2015 Dmitry Bakhvalov
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_MACOSX_H__
#define __HIREDIS_MACOSX_H__

#include <hiredict/adapters/macosx.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define RedisRunLoop RedictRunLoop

#define freeRedisRunLoop freeRedictRunLoop
#define redisMacOSAddRead redictMacOSAddRead
#define redisMacOSDelRead redictMacOSDelRead
#define redisMacOSAddWrite redictMacOSAddWrite
#define redisMacOSDelWrite redictMacOSDelWrite
#define redisMacOSCleanup redictMacOSCleanup
#define redisMacOSAsyncCallback redictMacOSAsyncCallback
#define redisMacOSAttach redictMacOSAttach

#endif

