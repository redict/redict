# Security Policy

All Redict users are encouraged to subscribe to the [redict-announce]
mailing list. This is a low-volume mailing list for posting important
announcements, including details of security vulnerabilities.

## Supported versions

The following Redict versions are supported and receive security updates:

* `7.3.x`: supported

Security fixes are generally backported to supported versions when practical.

## Reporting a security issue

If you believe you've discovered a serious vulnerability, please contact us
privately at [u.sircmpwn.redict-security@lists.sr.ht](u.sircmpwn.redict-security@lists.sr.ht)
to responsibly disclose details of the issue.

If you are a downstream distributor of Redict and have a reasonable
justification to have access to early disclosure of security-related issues,
please reach out to the security mailing list explaining your circumstances to
request early access to security disclosures.
