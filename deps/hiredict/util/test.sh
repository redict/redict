#!/bin/sh -ue

# SPDX-FileCopyrightText: 2024 Hiredict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-or-later

REDICT_SERVER=${REDICT_SERVER:-redict-server}
REDICT_PORT=${REDICT_PORT:-56379}
REDICT_SSL_PORT=${REDICT_SSL_PORT:-56443}
TEST_SSL=${TEST_SSL:-0}
SKIPS_AS_FAILS=${SKIPS_AS_FAILS:-0}
SSL_TEST_ARGS=
SKIPS_ARG=${SKIPS_ARG:-}
REDICT_DOCKER=${REDICT_DOCKER:-}

tmpdir=$(mktemp -d)
PID_FILE=${tmpdir}/hiredict-test-redict.pid
SOCK_FILE=${tmpdir}/hiredict-test-redict.sock

if [ "$TEST_SSL" = "1" ]; then
    SSL_CA_CERT=${tmpdir}/ca.crt
    SSL_CA_KEY=${tmpdir}/ca.key
    SSL_CERT=${tmpdir}/redict.crt
    SSL_KEY=${tmpdir}/redict.key

    openssl genrsa -out ${tmpdir}/ca.key 4096
    openssl req \
        -x509 -new -nodes -sha256 \
        -key ${SSL_CA_KEY} \
        -days 3650 \
        -subj '/CN=Hiredict Test CA' \
        -out ${SSL_CA_CERT}
    openssl genrsa -out ${SSL_KEY} 2048
    openssl req \
        -new -sha256 \
        -key ${SSL_KEY} \
        -subj '/CN=Hiredict Test Cert' | \
        openssl x509 \
            -req -sha256 \
            -CA ${SSL_CA_CERT} \
            -CAkey ${SSL_CA_KEY} \
            -CAserial ${tmpdir}/ca.txt \
            -CAcreateserial \
            -days 365 \
            -out ${SSL_CERT}

    SSL_TEST_ARGS="--ssl-host 127.0.0.1 --ssl-port ${REDICT_SSL_PORT} --ssl-ca-cert ${SSL_CA_CERT} --ssl-cert ${SSL_CERT} --ssl-key ${SSL_KEY}"
fi

cleanup() {
  if [ -n "${REDICT_DOCKER}" ] ; then
    docker kill redict-test-server
  else
    set +e
    kill $(cat ${PID_FILE})
  fi
  rm -rf ${tmpdir}
}
trap cleanup INT TERM EXIT

# base config
cat > ${tmpdir}/redict.conf <<EOF
pidfile ${PID_FILE}
port ${REDICT_PORT}
unixsocket ${SOCK_FILE}
unixsocketperm 777
EOF

# if not running in docker add these:
if [ ! -n "${REDICT_DOCKER}" ]; then
cat >> ${tmpdir}/redict.conf <<EOF
daemonize yes
enable-debug-command local
bind 127.0.0.1
EOF
fi

# if doing ssl, add these
if [ "$TEST_SSL" = "1" ]; then
    cat >> ${tmpdir}/redict.conf <<EOF
tls-port ${REDICT_SSL_PORT}
tls-ca-cert-file ${SSL_CA_CERT}
tls-cert-file ${SSL_CERT}
tls-key-file ${SSL_KEY}
EOF
fi

echo ${tmpdir}
cat ${tmpdir}/redict.conf
if [ -n "${REDICT_DOCKER}" ] ; then
    chmod a+wx ${tmpdir}
    chmod a+r ${tmpdir}/*
    docker run -d --rm --name redict-test-server \
        -p ${REDICT_PORT}:${REDICT_PORT} \
        -p ${REDICT_SSL_PORT}:${REDICT_SSL_PORT} \
        -v ${tmpdir}:${tmpdir} \
        ${REDICT_DOCKER} \
        redict-server ${tmpdir}/redict.conf
else
    ${REDICT_SERVER} ${tmpdir}/redict.conf
fi
# Wait until we detect the unix socket
echo waiting for server
while [ ! -S "${SOCK_FILE}" ]; do sleep 1; done

# Treat skips as failures if directed
[ "$SKIPS_AS_FAILS" = 1 ] && SKIPS_ARG="${SKIPS_ARG} --skips-as-fails"

${TEST_PREFIX:-} ./hiredict-test -h 127.0.0.1 -p ${REDICT_PORT} -s ${SOCK_FILE} ${SSL_TEST_ARGS} ${SKIPS_ARG}
