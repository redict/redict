/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef HIREDIS_LIBSDEVENT_H
#define HIREDIS_LIBSDEVENT_H

#include <hiredict/adapters/libsdevent.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define REDIS_LIBSDEVENT_DELETED REDICT_LIBSDEVENT_DELETED
#define REDIS_LIBSDEVENT_ENTERED REDICT_LIBSDEVENT_ENTERED

#define redisLibsdeventEvents redictLibsdeventEvents

#define redisLibsdeventDestroy redictLibsdeventDestroy
#define redisLibsdeventTimeoutHandler redictLibsdeventTimeoutHandler
#define redisLibsdeventHandler redictLibsdeventHandler
#define redisLibsdeventAddRead redictLibsdeventAddRead
#define redisLibsdeventDelRead redictLibsdeventDelRead
#define redisLibsdeventAddWrite redictLibsdeventAddWrite
#define redisLibsdeventDelWrite redictLibsdeventDelWrite
#define redisLibsdeventCleanup redictLibsdeventCleanup
#define redisLibsdeventSetTimeout redictLibsdeventSetTimeout
#define redisLibsdeventAttach redictLibsdeventAttach

#endif
