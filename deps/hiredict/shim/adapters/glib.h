/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_GLIB_H__
#define __HIREDIS_GLIB_H__

#include <hiredict/adapters/glib.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define RedisSource RedictSource

#define redis_source_add_read redict_source_add_read
#define redis_source_del_read redict_source_del_read
#define redis_source_add_write redict_source_add_write
#define redis_source_del_write redict_source_del_write
#define redis_source_cleanup redict_source_cleanup
#define redis_source_prepare redict_source_prepare
#define redis_source_check redict_source_check
#define redis_source_dispatch redict_source_dispatch
#define redis_source_finalize redict_source_finalize
#define redis_source_new redict_source_new

#endif /* __HIREDIS_GLIB_H__ */
