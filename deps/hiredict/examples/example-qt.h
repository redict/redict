// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#ifndef __HIREDICT_EXAMPLE_QT_H
#define __HIREDICT_EXAMPLE_QT_H

#include <hiredict/adapters/qt.h>

class ExampleQt : public QObject {

    Q_OBJECT

    public:
        ExampleQt(const char * value, QObject * parent = 0)
            : QObject(parent), m_value(value) {}

    signals:
        void finished();

    public slots:
        void run();

    private:
        void finish() { emit finished(); }

    private:
        const char * m_value;
        redictAsyncContext * m_ctx;
        RedictQtAdapter m_adapter;

    friend
    void getCallback(redictAsyncContext *, void *, void *);
};

#endif /* !__HIREDICT_EXAMPLE_QT_H */
