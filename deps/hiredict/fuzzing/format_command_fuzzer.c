/*
 * Copyright (c) 2020, Salvatore Sanfilippo <antirez at gmail dot com>
 * Copyright (c) 2020, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 * Copyright (c) 2020, Matt Stancliff <matt at genges dot com>,
 *                     Jan-Erik Rediger <janerik at fnordig dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
 * SPDX-FileCopyrightText: 2024 Pieter Noordhuis <pcnoordhuis at gmail dot com>
 * SPDX-FileCopyrightText: 2024 Matt Stancliff <matt at genges dot com>
 * SPDX-FileCopyrightText: 2024 Jan-Erik Rediger <janerik at fnordig dot com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#include <stdlib.h>
#include <string.h>
#include <hiredict/hiredict.h>

int LLVMFuzzerTestOneInput(const uint8_t *data, size_t size) {
    char *new_str, *cmd;

    if (size < 3)
        return 0;

    new_str = malloc(size+1);
    if (new_str == NULL)
        return 0;

    memcpy(new_str, data, size);
    new_str[size] = '\0';

    if (redictFormatCommand(&cmd, new_str) != -1)
        hi_free(cmd);

    free(new_str);
    return 0;
}
