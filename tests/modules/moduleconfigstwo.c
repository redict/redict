// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"
#include <strings.h>

/* Second module configs module, for testing.
 * Need to make sure that multiple modules with configs don't interfere with each other */
int bool_config;

int getBoolConfigCommand(const char *name, void *privdata) {
    REDICTMODULE_NOT_USED(privdata);
    if (!strcasecmp(name, "test")) {
        return bool_config;
    }
    return 0;
}

int setBoolConfigCommand(const char *name, int new, void *privdata, RedictModuleString **err) {
    REDICTMODULE_NOT_USED(privdata);
    REDICTMODULE_NOT_USED(err);
    if (!strcasecmp(name, "test")) {
        bool_config = new;
        return REDICTMODULE_OK;
    }
    return REDICTMODULE_ERR;
}

/* No arguments are expected */ 
int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    if (RedictModule_Init(ctx, "configs", 1, REDICTMODULE_APIVER_1) == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    if (RedictModule_RegisterBoolConfig(ctx, "test", 1, REDICTMODULE_CONFIG_DEFAULT, getBoolConfigCommand, setBoolConfigCommand, NULL, &argc) == REDICTMODULE_ERR) {
        return REDICTMODULE_ERR;
    }
    if (RedictModule_LoadConfigs(ctx) == REDICTMODULE_ERR) {
        return REDICTMODULE_ERR;
    }
    return REDICTMODULE_OK;
}