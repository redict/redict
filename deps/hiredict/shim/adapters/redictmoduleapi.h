/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_REDICTMODULEAPI_H__
#define __HIREDIS_REDICTMODULEAPI_H__

#include "redictmodule.h"

#include <hiredis/async.h>
#include <hiredis/hiredict.h>

#define redisModuleEvents redictModuleEvents

#define redisModuleReadEvent redictModuleReadEvent
#define redisModuleWriteEvent redictModuleWriteEvent
#define redisModuleAddRead redictModuleAddRead
#define redisModuleDelRead redictModuleDelRead
#define redisModuleAddWrite redictModuleAddWrite
#define redisModuleDelWrite redictModuleDelWrite
#define redisModuleStopTimer redictModuleStopTimer
#define redisModuleCleanup redictModuleCleanup
#define redisModuleTimeout redictModuleTimeout
#define redisModuleSetTimeout redictModuleSetTimeout
#define redisModuleCompatibilityCheck redictModuleCompatibilityCheck
#define redisModuleAttach redictModuleAttach

#endif
