# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

# Read the standard input and only shows dots in the output, filtering out
# all the other characters. Designed to avoid bufferization so that when
# we get the output of redict-trib and want to show just the dots, we'll see
# the dots as soon as redict-trib will output them.

fconfigure stdin -buffering none

while 1 {
    set c [read stdin 1]
    if {$c eq {}} {
        exit 0; # EOF
    } elseif {$c eq {.}} {
        puts -nonewline .
        flush stdout
    }
}
