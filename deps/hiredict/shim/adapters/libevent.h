/*
 * Copyright (c) 2010-2011, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_LIBEVENT_H__
#define __HIREDIS_LIBEVENT_H__

#include <hiredict/adapters/libevent.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define REDIS_LIBEVENT_DELETED REDICT_LIBEVENT_DELETED
#define REDIS_LIBEVENT_ENTERED REDICT_LIBEVENT_ENTERED

#define redisLibeventEvents redictLibeventEvents
#define redisLibeventDestroy redictLibeventDestroy
#define redisLibeventHandler redictLibeventHandler
#define redisLibeventUpdate redictLibeventUpdate
#define redisLibeventAddRead redictLibeventAddRead
#define redisLibeventDelRead redictLibeventDelRead
#define redisLibeventAddWrite redictLibeventAddWrite
#define redisLibeventDelWrite redictLibeventDelWrite
#define redisLibeventCleanup redictLibeventCleanup
#define redisLibeventSetTimeout redictLibeventSetTimeout
#define redisLibeventAttach redictLibeventAttach

#endif
