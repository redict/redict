/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_LIBUV_H__
#define __HIREDIS_LIBUV_H__

#include <hiredict/adapters/libuv.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define redisLibuvEvents redictLibuvEvents

#define redisLibuvPoll redictLibuvPoll
#define redisLibuvAddRead redictLibuvAddRead
#define redisLibuvDelRead redictLibuvDelRead
#define redisLibuvAddWrite redictLibuvAddWrite
#define redisLibuvDelWrite redictLibuvDelWrite
#define redisLibuvTimeout redictLibuvTimeout
#define redisLibuvSetTimeout redictLibuvSetTimeout
#define redisLibuvCleanup redictLibuvCleanup
#define redisLibuvAttach redictLibuvAttach

#endif
