// Copyright (c) 2019, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"
#include <pthread.h>
#include <errno.h>

#define UNUSED(V) ((void) V)

RedictModuleCtx *detached_ctx = NULL;

static int KeySpace_NotificationGeneric(RedictModuleCtx *ctx, int type, const char *event, RedictModuleString *key) {
    REDICTMODULE_NOT_USED(type);
    REDICTMODULE_NOT_USED(event);
    REDICTMODULE_NOT_USED(key);

    RedictModuleCallReply* rep = RedictModule_Call(ctx, "INCR", "c!", "notifications");
    RedictModule_FreeCallReply(rep);

    return REDICTMODULE_OK;
}

/* Timer callback. */
void timerHandler(RedictModuleCtx *ctx, void *data) {
    REDICTMODULE_NOT_USED(ctx);
    REDICTMODULE_NOT_USED(data);

    static int times = 0;

    RedictModule_Replicate(ctx,"INCR","c","timer");
    times++;

    if (times < 3)
        RedictModule_CreateTimer(ctx,100,timerHandler,NULL);
    else
        times = 0;
}

int propagateTestTimerCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleTimerID timer_id =
        RedictModule_CreateTimer(ctx,100,timerHandler,NULL);
    REDICTMODULE_NOT_USED(timer_id);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

/* Timer callback. */
void timerNestedHandler(RedictModuleCtx *ctx, void *data) {
    int repl = (long long)data;

    /* The goal is the trigger a module command that calls RM_Replicate
     * in order to test MULTI/EXEC structure */
    RedictModule_Replicate(ctx,"INCRBY","cc","timer-nested-start","1");
    RedictModuleCallReply *reply = RedictModule_Call(ctx,"propagate-test.nested", repl? "!" : "");
    RedictModule_FreeCallReply(reply);
    reply = RedictModule_Call(ctx, "INCR", repl? "c!" : "c", "timer-nested-middle");
    RedictModule_FreeCallReply(reply);
    RedictModule_Replicate(ctx,"INCRBY","cc","timer-nested-end","1");
}

int propagateTestTimerNestedCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleTimerID timer_id =
        RedictModule_CreateTimer(ctx,100,timerNestedHandler,(void*)0);
    REDICTMODULE_NOT_USED(timer_id);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

int propagateTestTimerNestedReplCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleTimerID timer_id =
        RedictModule_CreateTimer(ctx,100,timerNestedHandler,(void*)1);
    REDICTMODULE_NOT_USED(timer_id);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

void timerHandlerMaxmemory(RedictModuleCtx *ctx, void *data) {
    REDICTMODULE_NOT_USED(ctx);
    REDICTMODULE_NOT_USED(data);

    RedictModuleCallReply *reply = RedictModule_Call(ctx,"SETEX","ccc!","timer-maxmemory-volatile-start","100","1");
    RedictModule_FreeCallReply(reply);
    reply = RedictModule_Call(ctx, "CONFIG", "ccc!", "SET", "maxmemory", "1");
    RedictModule_FreeCallReply(reply);

    RedictModule_Replicate(ctx, "INCR", "c", "timer-maxmemory-middle");

    reply = RedictModule_Call(ctx,"SETEX","ccc!","timer-maxmemory-volatile-end","100","1");
    RedictModule_FreeCallReply(reply);
}

int propagateTestTimerMaxmemoryCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleTimerID timer_id =
        RedictModule_CreateTimer(ctx,100,timerHandlerMaxmemory,(void*)1);
    REDICTMODULE_NOT_USED(timer_id);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

void timerHandlerEval(RedictModuleCtx *ctx, void *data) {
    REDICTMODULE_NOT_USED(ctx);
    REDICTMODULE_NOT_USED(data);

    RedictModuleCallReply *reply = RedictModule_Call(ctx,"INCRBY","cc!","timer-eval-start","1");
    RedictModule_FreeCallReply(reply);
    reply = RedictModule_Call(ctx, "EVAL", "cccc!", "redict.call('set',KEYS[1],ARGV[1])", "1", "foo", "bar");
    RedictModule_FreeCallReply(reply);

    RedictModule_Replicate(ctx, "INCR", "c", "timer-eval-middle");

    reply = RedictModule_Call(ctx,"INCRBY","cc!","timer-eval-end","1");
    RedictModule_FreeCallReply(reply);
}

int propagateTestTimerEvalCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    RedictModuleTimerID timer_id =
        RedictModule_CreateTimer(ctx,100,timerHandlerEval,(void*)1);
    REDICTMODULE_NOT_USED(timer_id);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

/* The thread entry point. */
void *threadMain(void *arg) {
    REDICTMODULE_NOT_USED(arg);
    RedictModuleCtx *ctx = RedictModule_GetThreadSafeContext(NULL);
    RedictModule_SelectDb(ctx,9); /* Tests ran in database number 9. */
    for (int i = 0; i < 3; i++) {
        RedictModule_ThreadSafeContextLock(ctx);
        RedictModule_Replicate(ctx,"INCR","c","a-from-thread");
        RedictModuleCallReply *reply = RedictModule_Call(ctx,"INCR","c!","thread-call");
        RedictModule_FreeCallReply(reply);
        RedictModule_Replicate(ctx,"INCR","c","b-from-thread");
        RedictModule_ThreadSafeContextUnlock(ctx);
    }
    RedictModule_FreeThreadSafeContext(ctx);
    return NULL;
}

int propagateTestThreadCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    pthread_t tid;
    if (pthread_create(&tid,NULL,threadMain,NULL) != 0)
        return RedictModule_ReplyWithError(ctx,"-ERR Can't start thread");
    REDICTMODULE_NOT_USED(tid);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

/* The thread entry point. */
void *threadDetachedMain(void *arg) {
    REDICTMODULE_NOT_USED(arg);
    RedictModule_SelectDb(detached_ctx,9); /* Tests ran in database number 9. */

    RedictModule_ThreadSafeContextLock(detached_ctx);
    RedictModule_Replicate(detached_ctx,"INCR","c","thread-detached-before");
    RedictModuleCallReply *reply = RedictModule_Call(detached_ctx,"INCR","c!","thread-detached-1");
    RedictModule_FreeCallReply(reply);
    reply = RedictModule_Call(detached_ctx,"INCR","c!","thread-detached-2");
    RedictModule_FreeCallReply(reply);
    RedictModule_Replicate(detached_ctx,"INCR","c","thread-detached-after");
    RedictModule_ThreadSafeContextUnlock(detached_ctx);

    return NULL;
}

int propagateTestDetachedThreadCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    pthread_t tid;
    if (pthread_create(&tid,NULL,threadDetachedMain,NULL) != 0)
        return RedictModule_ReplyWithError(ctx,"-ERR Can't start thread");
    REDICTMODULE_NOT_USED(tid);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

int propagateTestSimpleCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    /* Replicate two commands to test MULTI/EXEC wrapping. */
    RedictModule_Replicate(ctx,"INCR","c","counter-1");
    RedictModule_Replicate(ctx,"INCR","c","counter-2");
    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

int propagateTestMixedCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    RedictModuleCallReply *reply;

    /* This test mixes multiple propagation systems. */
    reply = RedictModule_Call(ctx, "INCR", "c!", "using-call");
    RedictModule_FreeCallReply(reply);

    RedictModule_Replicate(ctx,"INCR","c","counter-1");
    RedictModule_Replicate(ctx,"INCR","c","counter-2");

    reply = RedictModule_Call(ctx, "INCR", "c!", "after-call");
    RedictModule_FreeCallReply(reply);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

int propagateTestNestedCommand(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    RedictModuleCallReply *reply;

    /* This test mixes multiple propagation systems. */
    reply = RedictModule_Call(ctx, "INCR", "c!", "using-call");
    RedictModule_FreeCallReply(reply);

    reply = RedictModule_Call(ctx,"propagate-test.simple", "!");
    RedictModule_FreeCallReply(reply);

    RedictModule_Replicate(ctx,"INCR","c","counter-3");
    RedictModule_Replicate(ctx,"INCR","c","counter-4");

    reply = RedictModule_Call(ctx, "INCR", "c!", "after-call");
    RedictModule_FreeCallReply(reply);

    reply = RedictModule_Call(ctx, "INCR", "c!", "before-call-2");
    RedictModule_FreeCallReply(reply);

    reply = RedictModule_Call(ctx, "keyspace.incr_case1", "c!", "asdf"); /* Propagates INCR */
    RedictModule_FreeCallReply(reply);

    reply = RedictModule_Call(ctx, "keyspace.del_key_copy", "c!", "asdf"); /* Propagates DEL */
    RedictModule_FreeCallReply(reply);

    reply = RedictModule_Call(ctx, "INCR", "c!", "after-call-2");
    RedictModule_FreeCallReply(reply);

    RedictModule_ReplyWithSimpleString(ctx,"OK");
    return REDICTMODULE_OK;
}

int propagateTestIncr(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    REDICTMODULE_NOT_USED(argc);
    RedictModuleCallReply *reply;

    /* This test propagates the module command, not the INCR it executes. */
    reply = RedictModule_Call(ctx, "INCR", "s", argv[1]);
    RedictModule_ReplyWithCallReply(ctx,reply);
    RedictModule_FreeCallReply(reply);
    RedictModule_ReplicateVerbatim(ctx);
    return REDICTMODULE_OK;
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    if (RedictModule_Init(ctx,"propagate-test",1,REDICTMODULE_APIVER_1)
            == REDICTMODULE_ERR) return REDICTMODULE_ERR;

    detached_ctx = RedictModule_GetDetachedThreadSafeContext(ctx);

    if (RedictModule_SubscribeToKeyspaceEvents(ctx, REDICTMODULE_NOTIFY_ALL, KeySpace_NotificationGeneric) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.timer",
                propagateTestTimerCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.timer-nested",
                propagateTestTimerNestedCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.timer-nested-repl",
                propagateTestTimerNestedReplCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.timer-maxmemory",
                propagateTestTimerMaxmemoryCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.timer-eval",
                propagateTestTimerEvalCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.thread",
                propagateTestThreadCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.detached-thread",
                propagateTestDetachedThreadCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.simple",
                propagateTestSimpleCommand,
                "",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.mixed",
                propagateTestMixedCommand,
                "write",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.nested",
                propagateTestNestedCommand,
                "write",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"propagate-test.incr",
                propagateTestIncr,
                "write",1,1,1) == REDICTMODULE_ERR)
            return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}

int RedictModule_OnUnload(RedictModuleCtx *ctx) {
    UNUSED(ctx);

    if (detached_ctx)
        RedictModule_FreeThreadSafeContext(detached_ctx);

    return REDICTMODULE_OK;
}
