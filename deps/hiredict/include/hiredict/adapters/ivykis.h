/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDICT_IVYKIS_H__
#define __HIREDICT_IVYKIS_H__
#include <iv.h>
#include <hiredict/hiredict.h>
#include <hiredict/async.h>

typedef struct redictIvykisEvents {
    redictAsyncContext *context;
    struct iv_fd fd;
} redictIvykisEvents;

static void redictIvykisReadEvent(void *arg) {
    redictAsyncContext *context = (redictAsyncContext *)arg;
    redictAsyncHandleRead(context);
}

static void redictIvykisWriteEvent(void *arg) {
    redictAsyncContext *context = (redictAsyncContext *)arg;
    redictAsyncHandleWrite(context);
}

static void redictIvykisAddRead(void *privdata) {
    redictIvykisEvents *e = (redictIvykisEvents*)privdata;
    iv_fd_set_handler_in(&e->fd, redictIvykisReadEvent);
}

static void redictIvykisDelRead(void *privdata) {
    redictIvykisEvents *e = (redictIvykisEvents*)privdata;
    iv_fd_set_handler_in(&e->fd, NULL);
}

static void redictIvykisAddWrite(void *privdata) {
    redictIvykisEvents *e = (redictIvykisEvents*)privdata;
    iv_fd_set_handler_out(&e->fd, redictIvykisWriteEvent);
}

static void redictIvykisDelWrite(void *privdata) {
    redictIvykisEvents *e = (redictIvykisEvents*)privdata;
    iv_fd_set_handler_out(&e->fd, NULL);
}

static void redictIvykisCleanup(void *privdata) {
    redictIvykisEvents *e = (redictIvykisEvents*)privdata;

    iv_fd_unregister(&e->fd);
    hi_free(e);
}

static int redictIvykisAttach(redictAsyncContext *ac) {
    redictContext *c = &(ac->c);
    redictIvykisEvents *e;

    /* Nothing should be attached when something is already attached */
    if (ac->ev.data != NULL)
        return REDICT_ERR;

    /* Create container for context and r/w events */
    e = (redictIvykisEvents*)hi_malloc(sizeof(*e));
    if (e == NULL)
        return REDICT_ERR;

    e->context = ac;

    /* Register functions to start/stop listening for events */
    ac->ev.addRead = redictIvykisAddRead;
    ac->ev.delRead = redictIvykisDelRead;
    ac->ev.addWrite = redictIvykisAddWrite;
    ac->ev.delWrite = redictIvykisDelWrite;
    ac->ev.cleanup = redictIvykisCleanup;
    ac->ev.data = e;

    /* Initialize and install read/write events */
    IV_FD_INIT(&e->fd);
    e->fd.fd = c->fd;
    e->fd.handler_in = redictIvykisReadEvent;
    e->fd.handler_out = redictIvykisWriteEvent;
    e->fd.handler_err = NULL;
    e->fd.cookie = e->context;

    iv_fd_register(&e->fd);

    return REDICT_OK;
}
#endif
