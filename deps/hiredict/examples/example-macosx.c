// Copyright (c) 2015 Dmitry Bakhvalov
//
// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Dmitry Bakhvalov
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <stdio.h>

#include <hiredict/hiredict.h>
#include <hiredict/async.h>
#include <hiredict/adapters/macosx.h>

void getCallback(redictAsyncContext *c, void *r, void *privdata) {
    redictReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* Disconnect after receiving the reply to GET */
    redictAsyncDisconnect(c);
}

void connectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Connected...\n");
}

void disconnectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    CFRunLoopStop(CFRunLoopGetCurrent());
    printf("Disconnected...\n");
}

int main (int argc, char **argv) {
    signal(SIGPIPE, SIG_IGN);

    CFRunLoopRef loop = CFRunLoopGetCurrent();
    if( !loop ) {
        printf("Error: Cannot get current run loop\n");
        return 1;
    }

    redictAsyncContext *c = redictAsyncConnect("127.0.0.1", 6379);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);
        return 1;
    }

    redictMacOSAttach(c, loop);

    redictAsyncSetConnectCallback(c,connectCallback);
    redictAsyncSetDisconnectCallback(c,disconnectCallback);

    redictAsyncCommand(c, NULL, NULL, "SET key %b", argv[argc-1], strlen(argv[argc-1]));
    redictAsyncCommand(c, getCallback, (char*)"end-1", "GET key");

    CFRunLoopRun();

    return 0;
}

