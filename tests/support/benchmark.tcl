# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

proc redictbenchmark_tls_config {testsdir} {
    set tlsdir [file join $testsdir tls]
    set cert [file join $tlsdir client.crt]
    set key [file join $tlsdir client.key]
    set cacert [file join $tlsdir ca.crt]

    if {$::tls} {
        return [list --tls --cert $cert --key $key --cacert $cacert]
    } else {
        return {}
    }
}

proc redictbenchmark {host port {opts {}}} {
    set cmd [list src/redict-benchmark -h $host -p $port]
    lappend cmd {*}[redictbenchmark_tls_config "tests"]
    lappend cmd {*}$opts
    return $cmd
}

proc redictbenchmarkuri {host port {opts {}}} {
    set cmd [list src/redict-benchmark -u redict://$host:$port]
    lappend cmd {*}[redictbenchmark_tls_config "tests"]
    lappend cmd {*}$opts
    return $cmd
}

proc redictbenchmarkuriuserpass {host port user pass {opts {}}} {
    set cmd [list src/redict-benchmark -u redict://$user:$pass@$host:$port]
    lappend cmd {*}[redictbenchmark_tls_config "tests"]
    lappend cmd {*}$opts
    return $cmd
}
