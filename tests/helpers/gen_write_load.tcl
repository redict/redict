# SPDX-FileCopyrightText: 2024 Redict Contributors
# SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
#
# SPDX-License-Identifier: BSD-3-Clause
# SPDX-License-Identifier: LGPL-3.0-only

source tests/support/redict.tcl

set ::tlsdir "tests/tls"

proc gen_write_load {host port seconds tls} {
    set start_time [clock seconds]
    set r [redict $host $port 1 $tls]
    $r client setname LOAD_HANDLER
    $r select 9
    while 1 {
        $r set [expr rand()] [expr rand()]
        if {[clock seconds]-$start_time > $seconds} {
            exit 0
        }
    }
}

gen_write_load [lindex $argv 0] [lindex $argv 1] [lindex $argv 2] [lindex $argv 3]
