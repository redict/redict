/*
 * Copyright (c) 2010-2011, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * All rights reserved.
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_LIBEV_H__
#define __HIREDIS_LIBEV_H__

#include <hiredict/adapters/libev.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define redisLibevEvents redictLibevEvents
#define redisLibevReadEvent redictLibevReadEvent
#define redisLibevWriteEvent redictLibevWriteEvent
#define redisLibevAddRead redictLibevAddRead
#define redisLibevDelRead redictLibevDelRead
#define redisLibevAddWrite redictLibevAddWrite
#define redisLibevDelWrite redictLibevDelWrite
#define redisLibevStopTimer redictLibevStopTimer
#define redisLibevCleanup redictLibevCleanup
#define redisLibevTimeout redictLibevTimeout
#define redisLibevSetTimeout redictLibevSetTimeout
#define redisLibevAttach redictLibevAttach

#endif
