// Copyright (c) 2009-2012, Salvatore Sanfilippo <antirez at gmail dot com>
// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

const char *ascii_logo =
"                                                                     \n"
"                     ##                                              \n"
"                 ########               Redict %s (%s/%d) %s bit\n"
"              ################                                       \n"
"          ########################      Running in %s mode\n"
"       ##############################   Port: %d\n"
"   ################################:    PID: %ld\n"
"  #############################:::::                                 \n"
"  ##::::####################::::::::                                 \n"
"  ##::::::::############::::::::::::          https://redict.io      \n"
"  ##:::::::::::+####+:::::::::::::::                                 \n"
"  ##::::::::::::::::::::::::::::::::                                 \n"
"  ##::::::::::::::::::::::::::::::::                                 \n"
"  ##::::::::::::::::::::::::::::::###                                \n"
"  ####::::::::::::::::::::::::####                                   \n"
"     #####:::::::::::::::::####                                      \n"
"         ####::::::::::####                                          \n"
"             ####::####                                              \n"
"                ####                                                 \n\n";
