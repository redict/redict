// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <hiredict/hiredict.h>
#include <hiredict/hiredict_ssl.h>
#include <hiredict/async.h>
#include <hiredict/adapters/libevent.h>

void getCallback(redictAsyncContext *c, void *r, void *privdata) {
    redictReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* Disconnect after receiving the reply to GET */
    redictAsyncDisconnect(c);
}

void connectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Connected...\n");
}

void disconnectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Disconnected...\n");
}

int main (int argc, char **argv) {
#ifndef _WIN32
    signal(SIGPIPE, SIG_IGN);
#endif

    struct event_base *base = event_base_new();
    if (argc < 5) {
        fprintf(stderr,
                "Usage: %s <key> <host> <port> <cert> <certKey> [ca]\n", argv[0]);
        exit(1);
    }

    const char *value = argv[1];
    size_t nvalue = strlen(value);

    const char *hostname = argv[2];
    int port = atoi(argv[3]);

    const char *cert = argv[4];
    const char *certKey = argv[5];
    const char *caCert = argc > 5 ? argv[6] : NULL;

    redictSSLContext *ssl;
    redictSSLContextError ssl_error = REDICT_SSL_CTX_NONE;

    redictInitOpenSSL();

    ssl = redictCreateSSLContext(caCert, NULL,
            cert, certKey, NULL, &ssl_error);
    if (!ssl) {
        printf("Error: %s\n", redictSSLContextGetError(ssl_error));
        return 1;
    }

    redictAsyncContext *c = redictAsyncConnect(hostname, port);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);
        return 1;
    }
    if (redictInitiateSSLWithContext(&c->c, ssl) != REDICT_OK) {
        printf("SSL Error!\n");
        exit(1);
    }

    redictLibeventAttach(c,base);
    redictAsyncSetConnectCallback(c,connectCallback);
    redictAsyncSetDisconnectCallback(c,disconnectCallback);
    redictAsyncCommand(c, NULL, NULL, "SET key %b", value, nvalue);
    redictAsyncCommand(c, getCallback, (char*)"end-1", "GET key");
    event_base_dispatch(base);

    redictFreeSSLContext(ssl);
    return 0;
}
