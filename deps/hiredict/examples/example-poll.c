// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <hiredict/async.h>
#include <hiredict/adapters/poll.h>

/* Put in the global scope, so that loop can be explicitly stopped */
static int exit_loop = 0;

void getCallback(redictAsyncContext *c, void *r, void *privdata) {
    redictReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* Disconnect after receiving the reply to GET */
    redictAsyncDisconnect(c);
}

void connectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        exit_loop = 1;
        return;
    }

    printf("Connected...\n");
}

void disconnectCallback(const redictAsyncContext *c, int status) {
    exit_loop = 1;
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }

    printf("Disconnected...\n");
}

int main (int argc, char **argv) {
    signal(SIGPIPE, SIG_IGN);

    redictAsyncContext *c = redictAsyncConnect("127.0.0.1", 6379);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);
        return 1;
    }

    redictPollAttach(c);
    redictAsyncSetConnectCallback(c,connectCallback);
    redictAsyncSetDisconnectCallback(c,disconnectCallback);
    redictAsyncCommand(c, NULL, NULL, "SET key %b", argv[argc-1], strlen(argv[argc-1]));
    redictAsyncCommand(c, getCallback, (char*)"end-1", "GET key");
    while (!exit_loop)
    {
        redictPollTick(c, 0.1);
    }
    return 0;
}
