// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <hiredict/hiredict.h>
#include <hiredict/async.h>
#include <hiredict/adapters/redictmoduleapi.h>

void debugCallback(redictAsyncContext *c, void *r, void *privdata) {
    (void)privdata; //unused
    redictReply *reply = r;
    if (reply == NULL) {
        /* The DEBUG SLEEP command will almost always fail, because we have set a 1 second timeout */
        printf("`DEBUG SLEEP` error: %s\n", c->errstr ? c->errstr : "unknown error");
        return;
    }
    /* Disconnect after receiving the reply of DEBUG SLEEP (which will not)*/
    redictAsyncDisconnect(c);
}

void getCallback(redictAsyncContext *c, void *r, void *privdata) {
    redictReply *reply = r;
    if (reply == NULL) {
        if (c->errstr) {
            printf("errstr: %s\n", c->errstr);
        }
        return;
    }
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* start another request that demonstrate timeout */
    redictAsyncCommand(c, debugCallback, NULL, "DEBUG SLEEP %f", 1.5);
}

void connectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Connected...\n");
}

void disconnectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Disconnected...\n");
}

/*
 * 1- Compile this file as a shared library. Directory of "redictmodule.h" must
 *    be in the include path.
 *       gcc -fPIC -shared -I../../redict/src/ -I.. example-redictmoduleapi.c -o example-redictmoduleapi.so
 *
 * 2- Load module:
 *       redict-server --loadmodule ./example-redictmoduleapi.so value
 */
int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {

    int ret = RedictModule_Init(ctx, "example-redictmoduleapi", 1, REDICTMODULE_APIVER_1);
    if (ret != REDICTMODULE_OK) {
        printf("error module init \n");
        return REDICTMODULE_ERR;
    }

    if (redictModuleCompatibilityCheck() != REDICT_OK) {
        return REDICTMODULE_ERR;
    }

    redictAsyncContext *c = redictAsyncConnect("127.0.0.1", 6379);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);
        return 1;
    }

    size_t len;
    const char *val = RedictModule_StringPtrLen(argv[argc-1], &len);

    RedictModuleCtx *module_ctx = RedictModule_GetDetachedThreadSafeContext(ctx);
    redictModuleAttach(c, module_ctx);
    redictAsyncSetConnectCallback(c,connectCallback);
    redictAsyncSetDisconnectCallback(c,disconnectCallback);
    redictAsyncSetTimeout(c, (struct timeval){ .tv_sec = 1, .tv_usec = 0});

    /*
    In this demo, we first `set key`, then `get key` to demonstrate the basic usage of the adapter.
    Then in `getCallback`, we start a `debug sleep` command to create 1.5 second long request.
    Because we have set a 1 second timeout to the connection, the command will always fail with a
    timeout error, which is shown in the `debugCallback`.
    */

    redictAsyncCommand(c, NULL, NULL, "SET key %b", val, len);
    redictAsyncCommand(c, getCallback, (char*)"end-1", "GET key");
    return 0;
}
