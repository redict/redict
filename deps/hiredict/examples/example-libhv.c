// SPDX-FileCopyrightText: 2024 Hiredict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-or-later

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <hiredict/hiredict.h>
#include <hiredict/async.h>
#include <hiredict/adapters/libhv.h>

void getCallback(redictAsyncContext *c, void *r, void *privdata) {
    redictReply *reply = r;
    if (reply == NULL) return;
    printf("argv[%s]: %s\n", (char*)privdata, reply->str);

    /* Disconnect after receiving the reply to GET */
    redictAsyncDisconnect(c);
}

void debugCallback(redictAsyncContext *c, void *r, void *privdata) {
    (void)privdata;
    redictReply *reply = r;

    if (reply == NULL) {
        printf("`DEBUG SLEEP` error: %s\n", c->errstr ? c->errstr : "unknown error");
        return;
    }

    redictAsyncDisconnect(c);
}

void connectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Connected...\n");
}

void disconnectCallback(const redictAsyncContext *c, int status) {
    if (status != REDICT_OK) {
        printf("Error: %s\n", c->errstr);
        return;
    }
    printf("Disconnected...\n");
}

int main (int argc, char **argv) {
#ifndef _WIN32
    signal(SIGPIPE, SIG_IGN);
#endif

    redictAsyncContext *c = redictAsyncConnect("127.0.0.1", 6379);
    if (c->err) {
        /* Let *c leak for now... */
        printf("Error: %s\n", c->errstr);
        return 1;
    }

    hloop_t* loop = hloop_new(HLOOP_FLAG_QUIT_WHEN_NO_ACTIVE_EVENTS);
    redictLibhvAttach(c, loop);
    redictAsyncSetTimeout(c, (struct timeval){.tv_sec = 0, .tv_usec = 500000});
    redictAsyncSetConnectCallback(c,connectCallback);
    redictAsyncSetDisconnectCallback(c,disconnectCallback);
    redictAsyncCommand(c, NULL, NULL, "SET key %b", argv[argc-1], strlen(argv[argc-1]));
    redictAsyncCommand(c, getCallback, (char*)"end-1", "GET key");
    redictAsyncCommand(c, debugCallback, NULL, "DEBUG SLEEP %d", 1);
    hloop_run(loop);
    hloop_free(&loop);
    return 0;
}
