/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 * Copyright (c) 2010-2011, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 * SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
 * SPDX-FileCopyrightText: 2024 Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDICT_ASYNC_PRIVATE_H
#define __HIREDICT_ASYNC_PRIVATE_H

#define _EL_ADD_READ(ctx)                                         \
    do {                                                          \
        refreshTimeout(ctx);                                      \
        if ((ctx)->ev.addRead) (ctx)->ev.addRead((ctx)->ev.data); \
    } while (0)
#define _EL_DEL_READ(ctx) do { \
        if ((ctx)->ev.delRead) (ctx)->ev.delRead((ctx)->ev.data); \
    } while(0)
#define _EL_ADD_WRITE(ctx)                                          \
    do {                                                            \
        refreshTimeout(ctx);                                        \
        if ((ctx)->ev.addWrite) (ctx)->ev.addWrite((ctx)->ev.data); \
    } while (0)
#define _EL_DEL_WRITE(ctx) do { \
        if ((ctx)->ev.delWrite) (ctx)->ev.delWrite((ctx)->ev.data); \
    } while(0)
#define _EL_CLEANUP(ctx) do { \
        if ((ctx)->ev.cleanup) (ctx)->ev.cleanup((ctx)->ev.data); \
        ctx->ev.cleanup = NULL; \
    } while(0)

static inline void refreshTimeout(redictAsyncContext *ctx) {
    #define REDICT_TIMER_ISSET(tvp) \
        (tvp && ((tvp)->tv_sec || (tvp)->tv_usec))

    #define REDICT_EL_TIMER(ac, tvp) \
        if ((ac)->ev.scheduleTimer && REDICT_TIMER_ISSET(tvp)) { \
            (ac)->ev.scheduleTimer((ac)->ev.data, *(tvp)); \
        }

    if (ctx->c.flags & REDICT_CONNECTED) {
        REDICT_EL_TIMER(ctx, ctx->c.command_timeout);
    } else {
        REDICT_EL_TIMER(ctx, ctx->c.connect_timeout);
    }
}

void __redictAsyncDisconnect(redictAsyncContext *ac);
void redictProcessCallbacks(redictAsyncContext *ac);

#endif  /* __HIREDICT_ASYNC_PRIVATE_H */
