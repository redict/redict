/*
 * Copyright (c) 2009-2011, Salvatore Sanfilippo <antirez at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_IVYKIS_H__
#define __HIREDIS_IVYKIS_H__

#include <hiredict/adapters/ivykis.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define redisIvykisEvents redictIvykisEvents
#define redisIvykisReadEvent redictIvykisReadEvent
#define redisIvykisWriteEvent redictIvykisWriteEvent
#define redisIvykisAddRead redictIvykisAddRead
#define redisIvykisDelRead redictIvykisDelRead
#define redisIvykisAddWrite redictIvykisAddWrite
#define redisIvykisDelWrite redictIvykisDelWrite
#define redisIvykisCleanup redictIvykisCleanup
#define redisIvykisAttach redictIvykisAttach

#endif
