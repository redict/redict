// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"
#include <math.h>
#include <errno.h>

/* ZSET.REM key element
 *
 * Removes an occurrence of an element from a sorted set. Replies with the
 * number of removed elements (0 or 1).
 */
int zset_rem(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 3) return RedictModule_WrongArity(ctx);
    RedictModule_AutoMemory(ctx);
    int keymode = REDICTMODULE_READ | REDICTMODULE_WRITE;
    RedictModuleKey *key = RedictModule_OpenKey(ctx, argv[1], keymode);
    int deleted;
    if (RedictModule_ZsetRem(key, argv[2], &deleted) == REDICTMODULE_OK)
        return RedictModule_ReplyWithLongLong(ctx, deleted);
    else
        return RedictModule_ReplyWithError(ctx, "ERR ZsetRem failed");
}

/* ZSET.ADD key score member
 *
 * Adds a specified member with the specified score to the sorted
 * set stored at key.
 */
int zset_add(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 4) return RedictModule_WrongArity(ctx);
    RedictModule_AutoMemory(ctx);
    int keymode = REDICTMODULE_READ | REDICTMODULE_WRITE;
    RedictModuleKey *key = RedictModule_OpenKey(ctx, argv[1], keymode);

    size_t len;
    double score;
    char *endptr;
    const char *str = RedictModule_StringPtrLen(argv[2], &len);
    score = strtod(str, &endptr);
    if (*endptr != '\0' || errno == ERANGE)
        return RedictModule_ReplyWithError(ctx, "value is not a valid float");

    if (RedictModule_ZsetAdd(key, score, argv[3], NULL) == REDICTMODULE_OK)
        return RedictModule_ReplyWithSimpleString(ctx, "OK");
    else
        return RedictModule_ReplyWithError(ctx, "ERR ZsetAdd failed");
}

/* ZSET.INCRBY key member increment
 *
 * Increments the score stored at member in the sorted set stored at key by increment.
 * Replies with the new score of this element.
 */
int zset_incrby(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 4) return RedictModule_WrongArity(ctx);
    RedictModule_AutoMemory(ctx);
    int keymode = REDICTMODULE_READ | REDICTMODULE_WRITE;
    RedictModuleKey *key = RedictModule_OpenKey(ctx, argv[1], keymode);

    size_t len;
    double score, newscore;
    char *endptr;
    const char *str = RedictModule_StringPtrLen(argv[3], &len);
    score = strtod(str, &endptr);
    if (*endptr != '\0' || errno == ERANGE)
        return RedictModule_ReplyWithError(ctx, "value is not a valid float");

    if (RedictModule_ZsetIncrby(key, score, argv[2], NULL, &newscore) == REDICTMODULE_OK)
        return RedictModule_ReplyWithDouble(ctx, newscore);
    else
        return RedictModule_ReplyWithError(ctx, "ERR ZsetIncrby failed");
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);
    if (RedictModule_Init(ctx, "zset", 1, REDICTMODULE_APIVER_1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "zset.rem", zset_rem, "write",
                                  1, 1, 1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "zset.add", zset_add, "write",
                                  1, 1, 1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "zset.incrby", zset_incrby, "write",
                                  1, 1, 1) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
