/*
 * Copyright (c) 2010-2011, Pieter Noordhuis <pcnoordhuis at gmail dot com>
 *
 * SPDX-FileCopyrightText: 2024 Hiredict Contributors
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * SPDX-License-Identifier: LGPL-3.0-or-later
 *
 */

#ifndef __HIREDIS_AE_H__
#define __HIREDIS_AE_H__

#include <hiredict/adapters/ae.h>
#include <hiredis/hiredis.h>
#include <hiredis/async.h>

#define redisAeEvents redictAeEvents
#define redisAeReadEvent redictAeReadEvent
#define redisAeWriteEvent redictAeWriteEvent
#define redisAeAddRead redictAeAddRead
#define redisAeDelRead redictAeDelRead
#define redisAeAddWrite redisAeAddWrite
#define redisAeDelWrite redictAeDelWrite
#define redisAeCleanup redictAeCleanup
#define redisAeAttach redictAeAttach

#endif
