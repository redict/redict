// SPDX-FileCopyrightText: 2024 Redict Contributors
// SPDX-FileCopyrightText: 2024 Salvatore Sanfilippo <antirez at gmail dot com>
//
// SPDX-License-Identifier: BSD-3-Clause
// SPDX-License-Identifier: LGPL-3.0-only

#include "redictmodule.h"
#include <pthread.h>
#include <assert.h>

#define UNUSED(V) ((void) V)

RedictModuleUser *user = NULL;

int call_without_user(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc < 2) {
        return RedictModule_WrongArity(ctx);
    }

    const char *cmd = RedictModule_StringPtrLen(argv[1], NULL);

    RedictModuleCallReply *rep = RedictModule_Call(ctx, cmd, "Ev", argv + 2, argc - 2);
    if (!rep) {
        RedictModule_ReplyWithError(ctx, "NULL reply returned");
    } else {
        RedictModule_ReplyWithCallReply(ctx, rep);
        RedictModule_FreeCallReply(rep);
    }
    return REDICTMODULE_OK;
}

int call_with_user_flag(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc < 3) {
        return RedictModule_WrongArity(ctx);
    }

    RedictModule_SetContextUser(ctx, user);

    /* Append Ev to the provided flags. */
    RedictModuleString *flags = RedictModule_CreateStringFromString(ctx, argv[1]);
    RedictModule_StringAppendBuffer(ctx, flags, "Ev", 2);

    const char* flg = RedictModule_StringPtrLen(flags, NULL);
    const char* cmd = RedictModule_StringPtrLen(argv[2], NULL);

    RedictModuleCallReply* rep = RedictModule_Call(ctx, cmd, flg, argv + 3, argc - 3);
    if (!rep) {
        RedictModule_ReplyWithError(ctx, "NULL reply returned");
    } else {
        RedictModule_ReplyWithCallReply(ctx, rep);
        RedictModule_FreeCallReply(rep);
    }
    RedictModule_FreeString(ctx, flags);

    return REDICTMODULE_OK;
}

int add_to_acl(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    if (argc != 2) {
        return RedictModule_WrongArity(ctx);
    }

    size_t acl_len;
    const char *acl = RedictModule_StringPtrLen(argv[1], &acl_len);

    RedictModuleString *error;
    int ret = RedictModule_SetModuleUserACLString(ctx, user, acl, &error);
    if (ret) {
        size_t len;
        const char * e = RedictModule_StringPtrLen(error, &len);
        RedictModule_ReplyWithError(ctx, e);
        return REDICTMODULE_OK;
    }

    RedictModule_ReplyWithSimpleString(ctx, "OK");

    return REDICTMODULE_OK;
}

int get_acl(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);

    if (argc != 1) {
        return RedictModule_WrongArity(ctx);
    }

    RedictModule_Assert(user != NULL);

    RedictModuleString *acl = RedictModule_GetModuleUserACLString(user);

    RedictModule_ReplyWithString(ctx, acl);

    RedictModule_FreeString(NULL, acl);

    return REDICTMODULE_OK;
}

int reset_user(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);

    if (argc != 1) {
        return RedictModule_WrongArity(ctx);
    }

    if (user != NULL) {
        RedictModule_FreeModuleUser(user);
    }

    user = RedictModule_CreateModuleUser("module_user");

    RedictModule_ReplyWithSimpleString(ctx, "OK");

    return REDICTMODULE_OK;
}

typedef struct {
    RedictModuleString **argv;
    int argc;
    RedictModuleBlockedClient *bc;
} bg_call_data;

void *bg_call_worker(void *arg) {
    bg_call_data *bg = arg;
    RedictModuleBlockedClient *bc = bg->bc;

    // Get Redict module context
    RedictModuleCtx *ctx = RedictModule_GetThreadSafeContext(bg->bc);

    // Acquire GIL
    RedictModule_ThreadSafeContextLock(ctx);

    // Set user
    RedictModule_SetContextUser(ctx, user);

    // Call the command
    size_t format_len;
    RedictModuleString *format_redict_str = RedictModule_CreateString(NULL, "v", 1);
    const char *format = RedictModule_StringPtrLen(bg->argv[1], &format_len);
    RedictModule_StringAppendBuffer(NULL, format_redict_str, format, format_len);
    RedictModule_StringAppendBuffer(NULL, format_redict_str, "E", 1);
    format = RedictModule_StringPtrLen(format_redict_str, NULL);
    const char *cmd = RedictModule_StringPtrLen(bg->argv[2], NULL);
    RedictModuleCallReply *rep = RedictModule_Call(ctx, cmd, format, bg->argv + 3, bg->argc - 3);
    RedictModule_FreeString(NULL, format_redict_str);

    /* Free the arguments within GIL to prevent simultaneous freeing in main thread. */
    for (int i=0; i<bg->argc; i++)
        RedictModule_FreeString(ctx, bg->argv[i]);
    RedictModule_Free(bg->argv);
    RedictModule_Free(bg);

    // Release GIL
    RedictModule_ThreadSafeContextUnlock(ctx);

    // Reply to client
    if (!rep) {
        RedictModule_ReplyWithError(ctx, "NULL reply returned");
    } else {
        RedictModule_ReplyWithCallReply(ctx, rep);
        RedictModule_FreeCallReply(rep);
    }

    // Unblock client
    RedictModule_UnblockClient(bc, NULL);

    // Free the Redict module context
    RedictModule_FreeThreadSafeContext(ctx);

    return NULL;
}

int call_with_user_bg(RedictModuleCtx *ctx, RedictModuleString **argv, int argc)
{
    UNUSED(argv);
    UNUSED(argc);

    /* Make sure we're not trying to block a client when we shouldn't */
    int flags = RedictModule_GetContextFlags(ctx);
    int allFlags = RedictModule_GetContextFlagsAll();
    if ((allFlags & REDICTMODULE_CTX_FLAGS_MULTI) &&
        (flags & REDICTMODULE_CTX_FLAGS_MULTI)) {
        RedictModule_ReplyWithSimpleString(ctx, "Blocked client is not supported inside multi");
        return REDICTMODULE_OK;
    }
    if ((allFlags & REDICTMODULE_CTX_FLAGS_DENY_BLOCKING) &&
        (flags & REDICTMODULE_CTX_FLAGS_DENY_BLOCKING)) {
        RedictModule_ReplyWithSimpleString(ctx, "Blocked client is not allowed");
        return REDICTMODULE_OK;
    }

    /* Make a copy of the arguments and pass them to the thread. */
    bg_call_data *bg = RedictModule_Alloc(sizeof(bg_call_data));
    bg->argv = RedictModule_Alloc(sizeof(RedictModuleString*)*argc);
    bg->argc = argc;
    for (int i=0; i<argc; i++)
        bg->argv[i] = RedictModule_HoldString(ctx, argv[i]);

    /* Block the client */
    bg->bc = RedictModule_BlockClient(ctx, NULL, NULL, NULL, 0);

    /* Start a thread to handle the request */
    pthread_t tid;
    int res = pthread_create(&tid, NULL, bg_call_worker, bg);
    assert(res == 0);

    return REDICTMODULE_OK;
}

int RedictModule_OnLoad(RedictModuleCtx *ctx, RedictModuleString **argv, int argc) {
    REDICTMODULE_NOT_USED(argv);
    REDICTMODULE_NOT_USED(argc);

    if (RedictModule_Init(ctx,"usercall",1,REDICTMODULE_APIVER_1)== REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"usercall.call_without_user", call_without_user,"write",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"usercall.call_with_user_flag", call_with_user_flag,"write",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "usercall.call_with_user_bg", call_with_user_bg, "write", 0, 0, 0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx, "usercall.add_to_acl", add_to_acl, "write",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"usercall.reset_user", reset_user,"write",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    if (RedictModule_CreateCommand(ctx,"usercall.get_acl", get_acl,"write",0,0,0) == REDICTMODULE_ERR)
        return REDICTMODULE_ERR;

    return REDICTMODULE_OK;
}
